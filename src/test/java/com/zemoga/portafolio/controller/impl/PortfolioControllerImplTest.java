package com.zemoga.portafolio.controller.impl;

import com.zemoga.portafolio.dto.PortfolioDTO;
import com.zemoga.portafolio.model.Portfolio;
import com.zemoga.portafolio.service.PortfolioService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig
class PortfolioControllerImplTest {

    @InjectMocks
    private PortfolioControllerImpl portfolioControllerImpl;
    @Mock
    private PortfolioService service;

    PortfolioDTO p1 = new PortfolioDTO();
    PortfolioDTO p2 = new PortfolioDTO();
    PortfolioDTO p3 = new PortfolioDTO();
    List<PortfolioDTO> list = List.of(p1,p2,p3);

    @Test
    void listPortfolio() {
        Mockito.when(service.getListPortfolio()).thenReturn(list);
        assertEquals(portfolioControllerImpl.listPortfolio().getStatusCode(), HttpStatus.OK);
    }

    @Test
    void listPortfolio_empty() {
        Mockito.when(service.getListPortfolio()).thenReturn(new ArrayList<PortfolioDTO>());
        assertEquals(portfolioControllerImpl.listPortfolio().getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    void portfolio() {
        Mockito.when(service.getPortfolio(Mockito.anyLong())).thenReturn(p1);
        assertEquals(portfolioControllerImpl.portfolio(5l).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void portfolio_null() {
        Mockito.when(service.getPortfolio(Mockito.anyLong())).thenReturn(null);
        assertEquals(portfolioControllerImpl.portfolio(5l).getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    void savePortfolio() {
        Mockito.when(service.savePortfolio(Mockito.any())).thenReturn(p1);
        assertEquals(portfolioControllerImpl.savePortfolio(p1).getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    void savePortfolio_null() {
        Mockito.when(service.savePortfolio(Mockito.any())).thenReturn(null);
        assertEquals(portfolioControllerImpl.savePortfolio(p1).getStatusCode(), HttpStatus.NOT_MODIFIED);
    }

    @Test
    void updatePortfolio() {
        Mockito.when(service.savePortfolio(Mockito.any())).thenReturn(p1);
        assertEquals(portfolioControllerImpl.updatePortfolio(p1).getStatusCode(), HttpStatus.OK);
    }


    @Test
    void updatePortfolio_null() {
        Mockito.when(service.savePortfolio(Mockito.any())).thenReturn(null);
        assertEquals(portfolioControllerImpl.updatePortfolio(p1).getStatusCode(), HttpStatus.NOT_MODIFIED);
    }
}