package com.zemoga.portafolio.service;

import com.zemoga.portafolio.dto.PortfolioDTO;
import com.zemoga.portafolio.model.Portfolio;
import com.zemoga.portafolio.repository.PortfolioRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig
class PortfolioServiceTest {

    @InjectMocks
    private PortfolioService portfolioService;
    @Mock
    private PortfolioRepository repository;
    @Mock
    private TwitterService twitterService;

    Portfolio p1 = new Portfolio();
    Portfolio p2 = new Portfolio();
    Portfolio p3 = new Portfolio();
    List<Portfolio> list = List.of(p1,p2,p3);

    @Test
    void getListPortfolio() {
        Mockito.when(repository.findAll()).thenReturn(list);
        assertEquals(portfolioService.getListPortfolio().size(), 3);
    }

    @Test
    void getPortfolio_null() {
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        assertNull(portfolioService.getPortfolio(5l));
    }

    @Test
    void getPortfolio() {
        p1.setId(5l);
        Mockito.when(repository.findById(Mockito.anyLong())).thenReturn(Optional.of(p1));
        assertEquals(portfolioService.getPortfolio(5l).getId(), p1.getId());
    }

    @Test
    void savePortfolio() {
        PortfolioDTO dto = new PortfolioDTO();
        p1.setId(1980l);
        Mockito.when(repository.save(Mockito.any())).thenReturn(p1);
        assertEquals(portfolioService.savePortfolio(dto).getId(), 1980l);
    }
}