package com.zemoga.portafolio.service;

import com.zemoga.portafolio.dto.PortfolioDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.social.twitter.api.TimelineOperations;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringJUnitConfig
class TwitterServiceTest {

    @InjectMocks
    private TwitterService twitterService;

    @Mock
    private TwitterService twitterService2;

    @Mock
    private Twitter twitter;


    private Tweet tw1 = new Tweet(1l, "idStr", "hello 1", new Date(), "fromUser",
            "profileImageUrl", 11l, 11l, "languageCode", "source");
    private Tweet tw2 = new Tweet(2l, "idStr", "hello 2", new Date(), "fromUser",
            "profileImageUrl", 22l, 22l, "languageCode", "source");
    private Tweet tw3 = new Tweet(3l, "idStr", "hello 3", new Date(), "fromUser",
            "profileImageUrl", 33l, 33l, "languageCode", "source");
    private List<Tweet> list = List.of(tw1, tw2, tw3);
    private String twittUserName = "elonmusk";

    @Test
    void getTweets_withData() {
        int pageSize = 0;
        TimelineOperations t =  Mockito.mock(TimelineOperations.class);
        Mockito.when(twitter.timelineOperations()).thenReturn(t);
        Mockito.when(t.getUserTimeline(Mockito.anyString(), Mockito.anyInt())).thenReturn(list);
        Assertions.assertEquals(twitterService.getTweets(twittUserName, pageSize), list);
    }

    @Test
    void getTweets_null() {

        int pageSize = 0;
        TimelineOperations t =  Mockito.mock(TimelineOperations.class);
        Mockito.when(twitter.timelineOperations()).thenReturn(t);
        Mockito.when(t.getUserTimeline(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
        Assertions.assertNull(twitterService.getTweets(twittUserName, pageSize));
    }


    @Test
    void getLastTweetsPorfolio_null() {
        PortfolioDTO port = new PortfolioDTO();
        port.setTwitUserName(twittUserName);
        int cant = 2;
        TimelineOperations t =  Mockito.mock(TimelineOperations.class);
        Mockito.when(twitter.timelineOperations()).thenReturn(t);
        Mockito.when(t.getUserTimeline(Mockito.anyString(), Mockito.anyInt())).thenReturn(null);
        twitterService.getLastTweetsPorfolio(port, cant);
        Assertions.assertNull(port.getTweets());
    }

    @Test
    void getLastTweetsPorfolio_0tweets() {
        PortfolioDTO port = new PortfolioDTO();
        port.setTwitUserName(twittUserName);
        int cant = 2;
        TimelineOperations t =  Mockito.mock(TimelineOperations.class);
        Mockito.when(twitter.timelineOperations()).thenReturn(t);
        Mockito.when(t.getUserTimeline(Mockito.anyString(), Mockito.anyInt())).thenReturn(new ArrayList<Tweet>());
        twitterService.getLastTweetsPorfolio(port, cant);
        Assertions.assertEquals(port.getTweets().size(), 0);
    }

    @Test
    void getLastTweetsPorfolio_1tweets() {
        PortfolioDTO port = new PortfolioDTO();
        port.setTwitUserName(twittUserName);
        int cant = 2;
        TimelineOperations t =  Mockito.mock(TimelineOperations.class);
        Mockito.when(twitter.timelineOperations()).thenReturn(t);
        Mockito.when(t.getUserTimeline(Mockito.anyString(), Mockito.anyInt())).thenReturn(list.subList(0,1));
        twitterService.getLastTweetsPorfolio(port, cant);
        Assertions.assertEquals(port.getTweets().size(), 1);
    }

    @Test
    void getLastTweetsPorfolio_2tweets() {
        PortfolioDTO port = new PortfolioDTO();
        port.setTwitUserName(twittUserName);
        int cant = 2;
        TimelineOperations t =  Mockito.mock(TimelineOperations.class);
        Mockito.when(twitter.timelineOperations()).thenReturn(t);
        Mockito.when(t.getUserTimeline(Mockito.anyString(), Mockito.anyInt())).thenReturn(list.subList(0,2));
        twitterService.getLastTweetsPorfolio(port, cant);
        Assertions.assertEquals(port.getTweets().size(), cant);
    }

    @Test
    void getLastTweetsPorfolio_3tweets() {
        PortfolioDTO port = new PortfolioDTO();
        port.setTwitUserName(twittUserName);
        int cant = 2;
        TimelineOperations t =  Mockito.mock(TimelineOperations.class);
        Mockito.when(twitter.timelineOperations()).thenReturn(t);
        Mockito.when(t.getUserTimeline(Mockito.anyString(), Mockito.anyInt())).thenReturn(list);
        twitterService.getLastTweetsPorfolio(port, cant);
        Assertions.assertEquals(port.getTweets().size(), cant);
    }
}