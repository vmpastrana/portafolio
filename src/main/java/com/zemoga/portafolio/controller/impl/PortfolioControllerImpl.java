package com.zemoga.portafolio.controller.impl;

import com.zemoga.portafolio.controller.IPortfolioController;
import com.zemoga.portafolio.dto.PortfolioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zemoga.portafolio.service.PortfolioService;

import java.util.List;

@RestController
public class PortfolioControllerImpl implements IPortfolioController {
	
	@Autowired
	private PortfolioService service;

	public ResponseEntity<List<PortfolioDTO>> listPortfolio() {
		List<PortfolioDTO> list = service.getListPortfolio();
		if(!list.isEmpty()) {
			return ResponseEntity.ok(list);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}

	public ResponseEntity<PortfolioDTO> portfolio(@PathVariable Long id) {
		PortfolioDTO port = service.getPortfolio(id);
		if(port!=null) {
			return ResponseEntity.ok(port);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}

	public ResponseEntity<PortfolioDTO> savePortfolio(@RequestBody PortfolioDTO dto) {
		PortfolioDTO port = service.savePortfolio(dto);
		if(port!=null){
			return ResponseEntity.status(HttpStatus.CREATED).body(port);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(null);
		}
	}

	public ResponseEntity<PortfolioDTO> updatePortfolio(@RequestBody PortfolioDTO dto) {
		PortfolioDTO port = service.savePortfolio(dto);
		if(port!=null){
			return ResponseEntity.ok(port);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(null);
		}
	}

}
