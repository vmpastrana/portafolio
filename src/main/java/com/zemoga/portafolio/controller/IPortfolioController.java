package com.zemoga.portafolio.controller;

import com.zemoga.portafolio.dto.PortfolioDTO;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")

@RequestMapping("/portfolios")
public interface IPortfolioController {

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Information found"),
            @ApiResponse(responseCode = "404", description = "Not information found"),
    })
    @GetMapping
    ResponseEntity<List<PortfolioDTO>> listPortfolio();

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Information about the person was found"),
            @ApiResponse(responseCode = "404", description = "Information about the person was not found"),
    })
    @GetMapping("/{id}")
    ResponseEntity<PortfolioDTO> portfolio(@PathVariable Long id);

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created person"),
            @ApiResponse(responseCode = "404", description = "Uncreated person"),
    })
    @PostMapping
    ResponseEntity<PortfolioDTO> savePortfolio(@RequestBody PortfolioDTO dto);

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated person"),
            @ApiResponse(responseCode = "404", description = "Unmodified person"),
    })
    @PutMapping
    ResponseEntity<PortfolioDTO> updatePortfolio(@RequestBody PortfolioDTO dto);
}
