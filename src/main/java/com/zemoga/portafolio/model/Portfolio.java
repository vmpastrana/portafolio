package com.zemoga.portafolio.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "portfolio")
public class Portfolio implements Serializable {

    @Id
    @Column(name = "idportfolio")
    private Long id;

    @Column(name = "image_url")
    private String image;

    @Column(name = "names")
    private String name;

    @Column(name = "last_names")
    private String lastName;

    @Column(name = "title")
    private String tittle;

    @Column(name = "description")
    private String description;

    @Column(name = "twitter_user_name")
    private String twittUserName;

//    @Column(name = "twitter_user_id")
//    private String twittUserId;

//    @Column(name = "user_id")
//    private String userId;

    @Column(name = "id")
    private String idDefault;

}