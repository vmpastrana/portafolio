package com.zemoga.portafolio.dto;

import com.zemoga.portafolio.model.Portfolio;
import lombok.Data;
import org.springframework.social.twitter.api.Tweet;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class PortfolioDTO implements Serializable {

    private Long id;
    private String image;
    private String name;
    private String lastName;
    private String tittle;
    private String description;
    private String twitUserName;
    private List<Tweet> tweets;

    public PortfolioDTO() {
    }

    public PortfolioDTO(Portfolio port){
        id = port.getId();
        image = port.getImage();
        name = port.getName();
        lastName = port.getLastName();
        tittle = port.getTittle();
        description = port.getDescription();
        twitUserName = port.getTwittUserName();
    }

    public static List<PortfolioDTO> listPortfolio(List<Portfolio> dto){
        return dto.stream().map(PortfolioDTO::new).collect(Collectors.toList());
    }

    public Portfolio portfolio(){
        Portfolio port = new Portfolio();
        port.setId(getId());
        port.setImage(getImage());
        port.setName(getName());
        port.setLastName(getLastName());
        port.setTittle(getTittle());
        port.setDescription(getDescription());
        port.setTwittUserName(getTwitUserName());
        if(getId()!=null) {
            port.setIdDefault(getId().toString());
        }
        return port;
    }

}
