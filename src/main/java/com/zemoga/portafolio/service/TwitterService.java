package com.zemoga.portafolio.service;

import com.zemoga.portafolio.dto.PortfolioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TwitterService {

    @Autowired
    private Twitter twitter;

    public List<Tweet> getTweets(String twittUserName, int pageSize) {
        return twitter.timelineOperations().getUserTimeline(twittUserName, pageSize);
    }

    public void getLastTweetsPorfolio(PortfolioDTO port, int cant) {
        List<Tweet> tweets = getTweets(port.getTwitUserName(), 0);
        if(tweets!=null) {
            if(tweets.size()>cant) {
                port.setTweets(tweets.subList(0, cant));
            } else {
                port.setTweets(tweets);
            }
        }
    }
}
