package com.zemoga.portafolio.service;

import com.zemoga.portafolio.dto.PortfolioDTO;
import com.zemoga.portafolio.model.Portfolio;
import com.zemoga.portafolio.repository.PortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PortfolioService {

	@Autowired
	private PortfolioRepository repository;

	@Autowired
	private TwitterService twitterService;

	public List<PortfolioDTO> getListPortfolio() {
		return PortfolioDTO.listPortfolio(repository.findAll());
	}

	public PortfolioDTO getPortfolio(Long id) {
		Portfolio port = repository.findById(id).orElse(null);
		if(port!=null) {
			PortfolioDTO p = new PortfolioDTO(port);
			twitterService.getLastTweetsPorfolio(p, 5);
			return p;
		}
		return null;
	}

	public PortfolioDTO savePortfolio(PortfolioDTO dto) {
		return new PortfolioDTO(repository.save(dto.portfolio()));
	}


}
